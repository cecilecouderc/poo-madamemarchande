// Créez une classe Produit : // nom // prix
// Créez une classe Panier avec :
// une méthode ajoute( produit )
// une méthode retire( produit )
// une proprieté totalHT // une proprieté totalTTC
// Utilisation : ajouter ce qu'il faut à ce code de base pour qu'il fonctionne.

    //Propriétés
function Produit (n, p){
    this.nom = n ; 
    this.prix = p ; 
}

function Panier (){
    this.listeCourses = [];
    this.totalHT = 0;
    this.totalTTC = 0 ;
    this.TVA = 1.05 ;

    //Méthode 
    this.ajouter = function(produit){
        this.totalHT = this.totalHT + produit.prix;
        this.totalTTC = this.totalHT*this.TVA; 
        this.listeCourses.push(produit.nom);
    };
}
    var baguette = new Produit( 'Baguette', 0.85); // prix HT
    var croissant = new Produit( 'Croissant', 0.80);

    var panier = new Panier();
    panier.ajouter(baguette);
    panier.ajouter(croissant);

    console.log(panier.totalHT);
    console.log(panier.totalTTC);   

    function ADeduire (){
        this.listeCourses = [];
        this.totalHT = 0;
        this.totalTTC = 0 ;
        this.TVA = 1.05 ;
        this.newTotalTTC = 0; 
    
        //Méthode 
        this.retirer = function(produit){
            this.totalHT = this.totalHT + produit.prix;
            this.totalTTC = this.totalHT * this.TVA;
            this.listeCourses.pop(produit.nom);
                };
        }        

    var aDeduire = new ADeduire();
    aDeduire.retirer(baguette); 
    
    var aPayer = panier.totalTTC - aDeduire.totalTTC ; 
    
    console.log(aDeduire.totalHT);
    console.log(aDeduire.totalTTC);   
    console.log(aPayer);

